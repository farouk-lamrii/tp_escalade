# TP Scala Social Network

Approche CRQS/Event Sourcing d'un réseau social avec 2 modules scala, Zookeeper, Kafka, MongoDB.

Gére les : Posts, Users, Comments, Like.


## Environnement Docker

Lancer le docker-compose pour lancer Kafka, Zookeeper et mongoDB :

`docker-compose up`


## Run API Scala

Build et run l'API Scala pour insertion des données :

`sbt`

`sbt> run`

`1`


## Run DataProcessing Scala 

Build et run DataProcessing pour traitement données et enregistrement MongoDB :

`sbt`

`sbt> run`

`2`


## Accès aux conteneurs 

On peut obtenir un shell sur les conteneurs avec les commandes suivantes :

`docker exec -it tpescalade_zookeeper_1 /bin/bash # zookeeper`

`docker exec -it tpescalade_kafka_1 /bin/bash # kafka`

`docker exec -it mongodb /bin/bash # mongodb`


## Commandes Kafka

Avec le shell sur le conteneur Kafka, on se rend dans le dossier bin/ :

`cd /opt/kafka/bin/`

Et on peut lister les topics :

`./kafka-topics --zookeeper zookeeper:2181 --list`

Créer un topic :

`./kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic posts`

Suppimer un topic :

`./kafka-topics.sh --zookeeper zookeeper:2181 --delete --topic posts`

Pour lire les messages d'un topic :

`./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic posts --from-beginning`


## Commande MongoDB

Avec le shell sur le conteneur MongoDB, on peut se connecter à la BDD :

`mongo`

On peut lister les databases et les collections :

`show databases`

`show collections`
